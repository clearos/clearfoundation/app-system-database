<?php

/**
 * System database class.
 *
 * @category   apps
 * @package    system-database
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2014 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/system_database/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\system_database;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('system_database');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\base\Configuration_File as Configuration_File;
use \clearos\apps\base\Daemon as Daemon;

clearos_load_library('base/Configuration_File');
clearos_load_library('base/Daemon');

// Exceptions
//-----------

use \clearos\apps\base\Engine_Exception as Engine_Exception;

clearos_load_library('base/Engine_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * System database class.
 *
 * @category   apps
 * @package    system-database
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2014 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/system_database/
 */

class System_Database extends Daemon
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const DB_HOST = '127.0.0.1';
    const DB_PORT = '3308';
    const DB_NUMBER = 95; // phpMyAdmin server ID
    const PATH_CONFIG = '/var/clearos/system_database';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    protected $config = NULL;
    protected $db_handle = NULL;

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * System database constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

        parent::__construct('system-mysqld');
    }

    /**
     * Returns password for specified database.
     *
     * @param string $database name
     *
     * @return string database password
     * @throws Engine_Exception
     */

    public function get_password($database)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config($database);

        return $this->config['password'];
    }

    /**
     * Runs a query.
     *
     * @param string $database name
     * @param string $query SQL query
     *
     * @return array query results
     * @throws Engine_Exception
     */

    public function run_query($database, $query)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_initialize_handle($database);
        $rows = [];

        $dbs = $this->db_handle->prepare($query);
        $dbs->execute();
        $rows = array();

        while ($row = $dbs->fetch())
            $rows[] = $row;

        return $rows;
    }

    /**
     * Runs database update.
     *
     * @param string $database name
     * @param string $query SQL query
     *
     * @return void
     * @throws Engine_Exception
     */

    public function run_update($database, $query)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_initialize_handle($database);

        $dbs = $this->db_handle->prepare($query);
        $dbs->execute();
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R I V A T E   M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a temporary table.
     *
     * @return void
     */

    protected function _initialize_handle($database)
    {
        clearos_profile(__METHOD__, __LINE__);

        $this->_load_config($database);

        $this->db_handle = new \PDO(
            'mysql:host=' . self::DB_HOST . ';port=' . self::DB_PORT . ';dbname=' . $database,
            $database,
            $this->config['password']
        );
    }

    /**
     * Loads database configuration file.
     *
     * @param string $database database name
     *
     * @return void
     * @throws Engine_Exception
     */

    protected function _load_config($database)
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new Configuration_File(self::PATH_CONFIG . '/' . $database, 'explode', '=', 2);

        if (!$file->exists())
            return $this->config = Array();

        $this->config = $file->load();
    }
}
