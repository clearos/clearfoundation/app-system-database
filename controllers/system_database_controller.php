<?php

/**
 * System database controller.
 *
 * @category   apps
 * @package    system-database
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2014 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/system_database/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\system_database\System_Database as System_Database;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * System database controller.
 *
 * @category   apps
 * @package    system-database
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2014 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/system_database/
 */

class System_Database_Controller extends ClearOS_Controller
{
    protected $db_name = NULL;
    protected $app_name = NULL;

    /**
     * System database constructor.
     *
     * @param string $db_name  database name
     * @param string $app_name app name
     *
     * @return view
     */

    function __construct($db_name, $app_name)
    {
        $this->db_name = $db_name;
        $this->app_name = $app_name;
    }

    /**
     * Default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('system_database');

        // Load the view data 
        //------------------- 

        try {
            $data['database_url'] = '/app/' . $this->app_name . '/database/login';
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        $this->page->view_form('system_database/overview', $data, lang('system_database_app_name'));
    }

    /**
     * phpMyAdmin signle sign-on.
     *
     * @return view
     */

    function login()
    {
        // Load dependencies
        //------------------

        $this->load->library('system_database/System_Database');

        // Login via phpMyAdmin single sign-on
        //------------------------------------

        session_name('SignonSession');
        session_start();

        $_SESSION['PMA_single_signon_user'] = $this->db_name;
        $_SESSION['PMA_single_signon_password'] = $this->system_database->get_password($this->db_name);

        $this->session->set_userdata('database_redirect', $this->app_name);

        session_write_close();

        header("Location: /mysql/index.php?server=" . System_Database::DB_NUMBER . "&db=$this->db_name");
    }
}
