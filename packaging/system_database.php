<?php

if (file_exists('/var/lib/system-mysql/mysql.sock')) {
    $cfg['Servers'][94]['host'] = '127.0.0.1';
    $cfg['Servers'][94]['port'] = '3308';
    $cfg['Servers'][94]['socket'] = '/var/lib/system-mysql/mysql.sock';
    $cfg['Servers'][94]['connect_type'] = 'tcp';
    $cfg['Servers'][94]['auth_type'] = 'cookie';
    $cfg['Servers'][94]['extension'] = 'mysqli';
    $cfg['Servers'][94]['verbose'] = 'System Database';

    $cfg['Servers'][95]['host'] = '127.0.0.1';
    $cfg['Servers'][95]['port'] = '3308';
    $cfg['Servers'][95]['socket'] = '/var/lib/system-mysql/mysql.sock';
    $cfg['Servers'][95]['connect_type'] = 'tcp';
    $cfg['Servers'][95]['auth_type'] = 'cookie';
    $cfg['Servers'][95]['extension'] = 'mysqli';
    $cfg['Servers'][95]['auth_type']  = 'signon';
    $cfg['Servers'][95]['SignonSession'] = 'SignonSession';
    $cfg['Servers'][95]['SignonURL'] = '/app/system_database/logout';
    $cfg['Servers'][95]['verbose'] = 'System Database - SSO';
}
